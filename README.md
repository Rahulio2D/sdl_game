# SDL_Game
SDL Game is a simple 2D puzzle game I developed as a way to learn more about C++ and SDL. In the game you play as an explorer searching for Golden Eggs in tombs filled with traps which disappear after a few seconds. Memorise the tomb layouts to reach the Golden Egg with as few deaths as possible.

## Controls
Use the WASD or Arrow keys to move the player in each direction.

## Installation
This project will unfortunately not run immediately after downloading the repository as I have not included the SDL Libraries. In order to get the project running, you will need to create a folder titled '_Libraries' within the root of the project, and in there place the latest development libraries for [SDL2](https://www.libsdl.org/download-2.0.php) and [SDL2_image](https://www.libsdl.org/projects/SDL_image/) - after downloading be sure to remove the version number for each folder. Lastly, you will also have to move the x64 DLL files to the root of the project, these are located in the 'lib' folder of each folder you've added to the '_Library' folder. As this installation procedure is fairly complicated, I've also set up a folder on my Google Drive which contains all the Libraries and DLLs already set up, so you can easily run it in Visual studio. This can be downloaded [here](https://drive.google.com/file/d/1MJGslvYaLp4M3meSA8cFm3JAqj54fjNL/view?usp=sharing).

## Credits
This project was worked on independently by myself ([Rahul Sharma](https://www.rahulsharma.uk)), with all the code being written by myself and some art assets designed by myself too. I also utilised one asset pack for the [dungeon environment](https://pixel-poem.itch.io/dungeon-assetpuck).

## Sidenote
This project was the first game I ever developed with C++ and SDL and therefore the code isn't very performant nor advanced. Although I was pleased with the quality of this game when I first developed it, since release I've learnt much more about C++ development and am actively working on developing higher quality games for my portfolio.
